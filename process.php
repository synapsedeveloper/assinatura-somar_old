<?php
require_once('config.php');

$completeName = $_POST["completeName"];
$comercialPhone = $_POST["comercialPhone"];
$emailAddress = $_POST["email"];
$cargo = $_POST["position"];
$treinamento = isset($_POST['treinamento']) ? $_POST['treinamento'] : null;
?>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Assinatura Concluída - Somar</title>
        <script src="js/script.js" type="text/javascript"></script>
        <script src="js/html2canvas.js" type="text/javascript"></script>
        <script src="js/canvas2image.js" type="text/javascript"></script>


        <!--Bootstrap Style / FontAwesome-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
        
        <style type="text/css">

            #signature {width: 505px; height: 110px; margin: 0 auto; background-color: white;}
            a:link {text-decoration:none; color:#002f68;}
            a:visited {text-decoration:none;}
            
            #screenshot {
              cursor: pointer;
              margin: 15px;
              padding: 8px 10px;
              background-color: #0a66da;
              border: none;
              font-size: 1.4em;
              color: white;
              border-radius: 5px;
            }
            #screenshot:focus {
              outline:0;
            }

            p:empty {
              display: none
            }
        </style>
    </head>
    <body>
        <div id="signature">
            <table width="505" height="96" border="0" background="images/fundo-globalvisa.png" style="background-size: contain; background-repeat: no-repeat; background-position: bottom center;">
            <tr>
              
            </tr>
                <tr>
                    <td width="265"></td>
                    <td align="left" width="240" style="font-weight: bold; padding-left: 10px; padding-top: 0px; padding-bottom: 0px; padding-right: 0px">
                      <h5 class="m-0" style="padding: 10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight: 400; font-size:18px; line-height: 20px; text-shadow: none; color:#002f68;">
                        <?php echo $completeName ?>
                      </h5>
                      <p class="m-0" style="padding-left: 10px; font-family: 'Roboto', sans-serif; font-weight: 400; font-size:16px; text-shadow: none; color:#0066ad;">
                        <?php echo $cargo ?>
                      </p>
                      <p class="m-0" style="padding-left: 10px; font-family: 'Roboto', sans-serif; font-weight: 400; font-size:14px; text-shadow: none; color:#0066ad;">
                        <?php echo $treinamento ?>
                      </p>
                      <p class="m-0" style="padding-left: 10px; font-family: 'Roboto', sans-serif; font-weight: 400; font-size:14px;  text-shadow: none; color: #0066ad;">
                        +55 <?php echo $comercialPhone ?>
                      </p>
                      <p class="m-0" style="font-family: 'Roboto', sans-serif; font-weight: 400; font-size:14px; padding-left: 10px; text-decoration:none; font-weight: normal; font-size:13px; text-shadow: none; color:#888b8d;">                     
                        <?php echo $emailAddress ?>@<?php echo $COMPANY_DOMAIN ?>
                      </p>  
                      <p class="m-0" style="padding-left: 10px; font-family: 'Roboto', sans-serif; font-weight: 400; font-size:13px; text-shadow: none; color:#888b8d;">
                        www.somarcontabilidade.com.br
                      </p>
                      
                    </td>
                </tr>
            </table>


            <br>
        </div>
        <div class="mt-5 text-center"> 
          <a id="screenshot" onclick="takeScreenShot()">Gerar Imagem</a>
        </div>
        <script>
        window.takeScreenShot = function() {
          var w = 505;
          var h = 140;
          var testcanvas = document.createElement('canvas');
          testcanvas.width = w * 3;
          testcanvas.height = h * 3;
          testcanvas.style.width = w + 'px';
          testcanvas.style.height = h + 'px';
          var context = testcanvas.getContext('2d');
          context.scale(3, 3);

          // Gerando a imagem
          html2canvas(document.getElementById("signature"), {
            onrendered: function(canvas) {
              //document.body.appendChild(canvas);

              var imgData = canvas.toDataURL("image/png");
              var img = document.createElement('a');
              img.setAttribute('download', "assinatura.png");
              img.setAttribute('id', "downloadimage");
              img.setAttribute("href", imgData);

              document.body.appendChild(img);
              var imageLink = document.getElementById("downloadimage");

              console.log(imageLink);
              imageLink.click();

            },
            width: 505,
            height: 140
          });
        }
        </script>
    </body>
  </html>

<?php
require_once('config.php');
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Gerador de Assinaturas - Somar</title>

    <!--Bootstrap Style / FontAwesome-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!--Custom CSS / Custom Font-->
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
    
</head>

<body>

<div class="container">
    <div class="page-header header">
        Gerador de Assinaturas
    </div>
    <form action="process.php" method="post">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="col-12 input-nome my-4">
                    <h4>Nome completo</h4>
                    <input type="text" class="form-control" name="completeName" placeholder="Utilize apenas Nome e Sobrenome">
                </div>
                <div class="col-12 input-cargo my-4">
                    <h4>Cargo</h4>
                    <input type="text" class="form-control" name="position" placeholder="Cargo">
                    <input type="checkbox" id="treinamento" name="treinamento" value="Em treinamento">
                    <label for="treinamento">Em treinamento</label>
                </div>
                <div class="col-12 input-tel my-4">
                    <h4>Telefone Comercial</h4>
                    <input type="text" class="form-control telefone" name="comercialPhone" id="telefone" class="telefone" placeholder="(__) ____-____">
                </div>
                <div class="col-12 input-email my-4">
                    <h4 id="teste">E-Mail</h4>
                    <div class="input-group">
                        <input type="text" class="form-control" name="email" id="email" placeholder="E-mail"
                            aria-describedby="basic-addon2">
                        <span class="input-group-addon" id="basic-addon2">@somacontabilidade.com.br</span>
                    </div>
                </div>
            </div>
        </div>
            
       
        <div class="row my-4">
            <div class="col-12 col-md-4 offset-md-4">
                <button type="submit" class="form-control btn btn-warning">Faça a minha assinatura!</button>
            </div>
        </div>
    </form>

    <div id="footer">
        <img width="280px" src="images/logo-footer.png"/>
    </div>
</div>

    <!-- Custom Scripts -->
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery.mask.min.js" type="text/javascript"></script>
        <script src="js/script.js" type="text/javascript"></script>
</body>
</html>
